// Normal

const newFunction = function literal(firstName, lastName) {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: function () {
            console.log(firstName + " " + lastName)
            return
        }
    }
}

newFunction("William", "Imoh").fullName()


// ES6

const newFunction = (firstName, lastName) => ({
    fullName: firstName + " " + lastName

})

console.log(newFunction("ah", "ji").fullName)